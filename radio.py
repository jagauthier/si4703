#!/usr/bin/python

import si4703
import time
import threading

def do_0a():
	while (1):
		fm.read_group0a(3)
		time.sleep(2)

def do_2a():
	while (1):
		fm.read_group2a(3)
		time.sleep(2)
		

fm = si4703.si4703()
fm.init()
fm.tune(93.7)

#threading.Thread(target=do_0a).start()
#threading.Thread(target=do_2a).start()

# This is all testing stuff.  I wanted to see if read_rds worked in a thread, and then dealth with changing
# stations

print "\033c"
loop=0
spaces="                           "
while 1:
	print "--------------------"
	#	fm.print_registry()
	print "Station     : " + str(fm.get_channel()) + spaces
	print "Stereo      : " + str(fm.STATUS_RSSI.ST) + spaces
	print "Strength    : " + str(fm.STATUS_RSSI.RSS) + spaces
	print "Tuning info : '" + fm.PS + "'" + spaces
	print "Radio text  : '" + fm.RadioText + "'" + spaces +spaces +spaces
	print "Type        : " + fm.pty +spaces
	print spaces + spaces
	print "\033[0;0H"
	time.sleep(.5)
	
	loop=loop+1
	if loop % 20 == 0:
		if str(fm.get_channel())!="107.7":
			fm.seek_right()
			print "\033c"			
	